import React from 'react';
import Logo from '../Logo/Logo';
import classes from './Toolbar.css';
import NavigationItems from '../NavigationItems/NavigationItems';
import MenuButton from '../MenuButton/MenuButton';

const Toolbar = (props) => (
    <header className={classes.Toolbar}>
        <MenuButton clicked={props.menuButtonClicked}/>
        <div className={classes.Logo}>
            <Logo />
        </div>        
        <nav className={classes.DesktopOnly}>
            <NavigationItems />
        </nav>
    </header>
);

export default Toolbar;