import React from 'react';
import classes from './Logo.css';
import burgerLogo from '../../../assets/logo.png';

const Logo = (props) => (
    <div className={classes.Logo}>
        <img src={burgerLogo} alt="BurgerLogo"/>
    </div>
);

export default Logo;