import React, {Component} from 'react';
import Aux from '../../../hoc/Auxiliary';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {
    /*an alternative is use memo*/
    componentDidUpdate() {
        console.log('[OrderSummary] DidUpdate')
    }

    render() {
        const ingredientSummary = Object.keys(this.props.ingredients)
        .map(igKey => {
            return <li key={igKey}><span style={{textTransform: 'capitalize'}}>{igKey}</span>: {this.props.ingredients[igKey]}</li>
        });

        return(
            <Aux>
                <h3>Your Order</h3>
                <p>Your burger contains following:</p>
                <ul>{ingredientSummary}</ul>
                <p>Continue to checkout?</p>
                <p><strong>Total price: ${this.props.price.toFixed(2)}</strong></p>
                <Button btnType="Danger" clicked={this.props.purchaseCanceled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </Aux>
        );
    };
};

export default OrderSummary;